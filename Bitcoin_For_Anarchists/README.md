This slides can be visited [here](https://ekaitz-zarraga.gitlab.io/talks/Bitcoin_For_Anarchists/)

There are some exceptions to the license in this folder.
Some of the pictures are obtained from websites, they are referenced in the
presentation as they should be.

The images with no reference are made by myself and they are CCBY too.
