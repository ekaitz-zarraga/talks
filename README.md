# Talks

This repository contains all the talks I made and I want to publish.

Each folder is one talk.

Gitlab pages is automated to create the talks in the following address:

```
https://ekaitz-zarraga.gitlab.io/talks/${NAME_OF_THE_FOLDER}
```

# LICENSE

All the functional part of the talks is released under the terms of the MIT
license. This includes the redistributed parts of the framework (reveal-js) and
all the external parts added by myself like `gitlab-ci`.

The content of the talks is released under CCBY license which is included in
the LICENSE file.
